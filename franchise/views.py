import random

from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from franchise.models import Project, ProjectRating
from room.models import Room


@login_required(login_url="/login")
def project_page(request, project_id=None):
    if project_id is not None:
        # returns single specific project (franchise)
        project = Project.objects.get(id=project_id)
        try:
            project_rating = ProjectRating.objects.get(project=project, user=request.user).value
        except ProjectRating.DoesNotExist:
            project_rating = None
        project_rooms = Room.objects.filter(project=project)
        slider_rooms = sorted(project_rooms.distinct(),
                              key=lambda x: random.random())[:5]
        return render(request, "franchise/projectPage.html",
                      {"project": project, "project_rating": project_rating, "project_rooms": project_rooms,
                       "slider_rooms": slider_rooms})
    else:
        # returns list of all projects (franchises)
        # TODO pagination of projects
        # TODO order_by() rating (desk)
        # project_rating = ProjectRating.objects.all()
        projects = Project.objects.all() # .order_by()
        slider_projects = sorted(projects.distinct(),
                                 key=lambda x: random.random())[:5]
        return render(request, "franchise/allProjects.html", {"projects": projects, "slider_projects": slider_projects})