from django.conf.urls import url

urlpatterns = [
    url(r'(?P<project_id>\d+)?(/)?$', 'franchise.views.project_page', name='project'),
]
