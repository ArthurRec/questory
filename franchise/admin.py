from django.contrib import admin

from franchise.models import Project, ProjectRating

admin.site.register(Project)
admin.site.register(ProjectRating)

