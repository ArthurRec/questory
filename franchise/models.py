import os

from django.contrib.auth.models import User
from django.db import models


def get_path(instance, filename):
    return os.path.join("projects", instance.name, filename)


class Project(models.Model):  # Franchise
    class Meta:
        db_table = "project"
        verbose_name_plural = "projects"
        unique_together = ('name', 'site')

    image = models.ImageField(blank=True, upload_to=get_path)
    name = models.CharField(max_length=40)
    about = models.TextField(max_length=1050, blank=True)
    phone = models.CharField(max_length=15)
    site = models.URLField()
    project_rating = models.ManyToManyField(User, through="ProjectRating")

    def __unicode__(self):
        return "project name: %s" % self.name


class ProjectRating(models.Model):
    class Meta:
        db_table = "project rating"
        verbose_name_plural = "project ratings"
        unique_together = ('user', 'project')

    user = models.ForeignKey(User, null=True)
    project = models.ForeignKey('Project', null=True)
    value = models.PositiveSmallIntegerField()

    def __unicode__(self):
        return "rating: %s" % self.value
