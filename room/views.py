import random
from datetime import datetime

from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.http import HttpResponseRedirect as redirect
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.generic import ListView

from account.forms import ReviewForm
from account.models import Review
from franchise.models import Project
from room.models import Room, RoomRating


def home_page(request):
    # TODO pagination of rooms
    rooms = Room.objects.all()
    # projects = Project.objects.all()
    slider_rooms = sorted(rooms.distinct(),
                          key=lambda x: random.random())[:10]
    return render(request, "room/homepage.html", {"rooms": rooms, "slider_rooms": slider_rooms})


@login_required(login_url="/login")
def rooms_page(request, project_id=None, room_id=None):
    if room_id is None and project_id is not None:
        # TODO pagination of articles
        project = Project.objects.get(id=project_id)
        project_rooms = Room.objects.filter(project=project)  # .order_by(RoomRating.value)
        # project_rooms = Project.objects.get(project__id=project_id).rooms_list.order_by(RoomRating.value)
        return render(request, "room/projectRooms.html", {"rooms": project_rooms})
    elif room_id is not None and project_id is not None:
        user = request.user
        form = ReviewForm()
        room = Room.objects.get(id=room_id)
        # TODO pagination of comments
        reviews = Review.objects.filter(room=room)
        try:
            room_rating = RoomRating.objects.get(user=user, room=room_id)  # of current user
        except RoomRating.DoesNotExist:
            room_rating = None
        if room is not None:
            return render(request, "room/roomPage.html",
                          {"room": room, "user": user, "room_rating": room_rating,
                           "review_form": form, "reviews": reviews})
        else:
            return HttpResponse("No such article!")
    else:
        # TODO
        pass


class FavouriteRoomsView(ListView):
    model = Room
    template_name = "room/favouriteRooms.html"

    @method_decorator(login_required(login_url="/login"))
    def dispatch(self, request, *args, **kwargs):
        return super(FavouriteRoomsView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(FavouriteRoomsView, self).get_context_data(**kwargs)
        context['favourite_rooms'] = Room.objects.filter(users_favour_list=self.request.user)
        return context


class VisitedRoomsView(ListView):
    model = Room
    template_name = "room/savedArticles.html"

    @method_decorator(login_required(login_url="/login"))
    def dispatch(self, request, *args, **kwargs):
        return super(VisitedRoomsView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(VisitedRoomsView, self).get_context_data(**kwargs)
        context['visited_rooms'] = Room.objects.filter(users_visit_list=self.request.user)
        return context


# TODO by AJAX
@login_required(login_url="/login")
def remove_favourite_room(request, room_id=None):
    user = request.user
    favourite_rooms = user.profile.favourite_rooms.all()
    if room_id is not None:
        room_to_delete = favourite_rooms.get(id=room_id)
        # < -- remove added in favour room for current user without deleting room model from database -- >
        user.profile.favourite_rooms.remove(room_to_delete)
    else:
        user.profile.favourite_rooms.clear()
    return redirect(reverse("rooms:favourite"))


# TODO by AJAX
@login_required(login_url="/login")
def remove_visited_room(request, room_id=None):
    user = request.user
    visited_rooms = user.profile.visited_rooms.all()
    if room_id is not None:
        room_to_delete = visited_rooms.get(id=room_id)
        # < -- remove added in visited room for current user without deleting room model from database -- >
        user.profile.visited_rooms.remove(room_to_delete)
    else:
        user.profile.visited_rooms.clear()
    return redirect(reverse("rooms:visited"))


@login_required(login_url="/login")
def ajax_post_review(request):
    user = request.user
    if request.method == "POST" and request.is_ajax():
        text = request.POST.get('text')
        room_id = request.POST.get('room_id')
        room = Room.objects.get(id=room_id)
        review = Review.objects.create(
            text=text,
            author=user,
            room=room,
            pub_date=datetime.now()
        )
        return render(request, "account/commentInclude.html", {"review": review})
    else:
        return render(request, "room/oneArticle.html")


@login_required(login_url="/login")
def ajax_remove_review(request):
    user = request.user
    if request.method == "POST" and request.is_ajax():
        review_id = request.POST.get('review_id')
        review = Review.objects.get(id=review_id)
        review.delete()
        return HttpResponse("ok")
