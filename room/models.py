import os

from django.contrib.auth.models import User
from django.db import models


def get_path(instance, filename):
    return os.path.join("rooms", instance.name, filename)


class Room(models.Model):
    class Meta:
        db_table = "room"
        verbose_name_plural = "rooms"

    image = models.ImageField(blank=True, upload_to=get_path)
    name = models.CharField(max_length=40)
    about = models.TextField(max_length=1000)
    address = models.CharField(max_length=100)
    phone = models.CharField(max_length=15)
    project = models.ForeignKey('franchise.Project', related_name="rooms_list", null=True)
    room_rating = models.ManyToManyField(User, through="RoomRating")

    def __unicode__(self):
        return "room name: %s (%s)" % (self.name, self.project.name)


class RoomRating(models.Model):
    class Meta:
        db_table = "room rating"
        verbose_name_plural = "room ratings"
        unique_together = ('user', 'room')

    user = models.ForeignKey(User, null=True)
    room = models.ForeignKey('Room', null=True)
    value = models.PositiveSmallIntegerField()

    def __unicode__(self):
        return "rating: %s (room %s(%s)) from user %s" % (
            self.value, self.room.name, self.room.project.name, self.user.username)
