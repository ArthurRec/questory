from django.conf.urls import url

from room.views import FavouriteRoomsView, VisitedRoomsView

urlpatterns = [
    url(r'^(?P<project_id>\d+)/(?P<room_id>\d+)?(/)?$', 'room.views.rooms_page', name='room'),
    url(r'^favourite/$', FavouriteRoomsView.as_view(), name='favourite'),
    url(r'^visited/$', VisitedRoomsView.as_view(), name='visited'),
    url(r'^favourite/remove/(?P<room_id>\d+)?(/)?$', 'room.views.remove_favourite_room', name='remove_favourite_room'),
    url(r'^visited/remove/(?P<room_id>\d+)?(/)?$', 'room.views.remove_visited_room', name='remove_visited_room'),
    url(r'^review/add/$', 'room.views.ajax_post_review', name="post_review"),
    url(r'^review/remove/$', 'room.views.ajax_remove_review', name="remove_review"),
]
