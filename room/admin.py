from django.contrib import admin

from room.models import Room, RoomRating

admin.site.register(Room)
admin.site.register(RoomRating)
