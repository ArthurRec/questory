$(document).ready(function () {
    $("#password").keyup(function (event) {
        if ($(this).val().length < 8 && $(this).val().length >= 1) {
            $(this).css("border", "2px solid red")
        } else if (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/.test($(this).val())) {
            $(this).css("border", "2px solid green")
        } else if ($(this).val().length > 0) {
            $(this).css("border", "2px solid yellow")
        } else {
            $(this).css("border", "2px solid normal")
        }
    });

    $("#conf_password").keyup(function (event) {
        if ($(this).val() == $("#password").val() && $("#conf_password").val().length >= 8) {
            $(this).css("border", "2px solid green");
        } else if ($(this).val().length > 0) {
            $(this).css("border", "2px solid red")
        } else {
            $(this).css("border", "2px solid normal")
        }
    });


    $("#register-form").validate({

        rules: {
            username: {
                required: true,
                minlength: 5,
                maxlength: 14
            },
            email: {
                required: true,
                email: true,
                maxlength: 30
            },
            password: {
                required: true
            },
            confirm_password: {
                required: true
            }
        },

        messages: {
            username: {
                required: "required field",
                minlength: "username should have at least 5 characters",
                maxlength: "a maximum length of username 14 characters"
            },
            email: {
                email: "incorrect email",
                required: "required field",
                maxlength: "a maximum length of email 30 characters"
            },
            password: {
                required: "required field"
            },
            confirm_password: {
                required: "required field"
            }
        }

    });


    $("#login-form").validate({

        rules: {
            login: {
                required: true,
                minlength: 5,
                maxlength: 14
            },
            password: {
                required: true,
                minlength: 8,
                maxlength: 18
            }
        },

        messages: {
            login: {
                required: "required field",
                minlength: "login should have at least 5 characters",
                maxlength: "a maximum length of login 14 characters"
            },
            password: {
                required: "required field",
                minlength: "password should have at least 8 characters",
                maxlength: "a maximum length of password 18 characters"
            }
        }

    });

});
