"""queStory URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin

from questory import settings

urlpatterns = [
    url(r'^$', 'room.views.home_page', name="homepage"),
    url(r'^rooms/', include('room.urls', namespace="rooms")),               # rooms (room.urls) LIKE ARTICLES
    url(r'^projects/', include('franchise.urls', namespace="projects")),    # project page (franchise.urls) LIKE JOURNAL
    url(r'^admin/', include(admin.site.urls)),
    url(r'^login/$', 'account.views.login', name='login'),
    url(r'^logout/$', 'account.views.logout', name="logout"),
    url(r'^register/$', 'account.views.register', name="register"),
    url(r'^user/', include('account.urls', namespace="profiles")),
    url(r'^contact/$', 'account.views.contact_page', name="contact"),
    url(r'^about/$', 'account.views.about_page', name="about"),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
