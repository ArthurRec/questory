from django.conf.urls import url

from account.views import EditProfileView

urlpatterns = [
    url(r'^my_profile$', EditProfileView.as_view(), name="edit_my_profile"),
    url(r'(?P<user_id>\d+)?(/)?$', 'account.views.profile', name='profile'),
]
