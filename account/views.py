from django.contrib.auth import login as auth_login, logout as auth_logout, authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import HttpResponseRedirect as redirect
from django.shortcuts import render
from django.views.generic import UpdateView

from account.forms import LoginForm, RegisterForm, EditProfileForm
from account.models import Profile


def login(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            user = authenticate(
                username=form.cleaned_data["login"],
                password=form.cleaned_data["password"]
            )
            if user is not None:
                auth_login(request, user)
                if "next" in request.GET:
                    if request.GET["next"] != "/logout/":  # kam_kost
                        return redirect(request.GET["next"])
                    else:
                        return redirect(reverse("homepage"))
                else:
                    return redirect(reverse("homepage"))
            else:
                return render(request, "account/signin.html", {"form": form,
                                                                "errors": ["Incorrect data!"]})
        else:
            return render(request, "account/signin.html", {"form": form})
    else:
        form = LoginForm()
        return render(request, "account/signin.html", {"form": form})


@login_required(login_url="/login")  # <- reverse too?
def logout(request):
    auth_logout(request)
    return redirect(reverse("login"))


def register(request):
    if request.method == "POST":
        form = RegisterForm(request.POST)
        if form.is_valid():
            user = User.objects.create_user(username=form.cleaned_data['username'], email=form.cleaned_data['email'],
                                            password=form.cleaned_data['password'])
            user.is_staff = False
            Profile.objects.create(user=user)
            user.save()
            return redirect(reverse("login"))
        else:
            return render(request, "account/register.html",
                          {"form": form})
    else:
        return render(request, "account/register.html",
                      {"form": RegisterForm()})


def about_page(request):
    if request.method == "POST":
        pass
    else:
        return render(request, "account/about.html")


def contact_page(request):
    if request.method == "POST":
        pass
    else:
        return render(request, "account/contact.html")


@login_required(login_url="/login")
def profile(request, user_id):  # user_id=None
    # if user_id is None:
    #     pass  # TODO 404 or (user is not found) -----------------
    # else:
    user = User.objects.get(id=user_id)
    return render(request, "account/profile.html", {"user": user})


class EditProfileView(UpdateView):
    form_class = EditProfileForm
    model = Profile
    template_name = "account/myProfileSettings.html"
    success_url = reverse_lazy("profiles:edit_my_profile")

    def get_object(self, queryset=None):
        return Profile.objects.get(user__username=self.request.user)
