from django.contrib import admin

from account.models import Profile, Contact, Review

admin.site.register(Profile)
admin.site.register(Contact)
admin.site.register(Review)

