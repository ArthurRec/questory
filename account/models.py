import os

from django.contrib.auth.models import User
from django.db import models


def get_path(instance, filename):
    return os.path.join("users", instance.user.username, filename)


class Profile(models.Model):
    class Meta:
        db_table = "profiles"
        verbose_name_plural = "profiles"

    user = models.OneToOneField(User)

    MALE = "M"
    FEMALE = "F"
    genders = (
        (MALE, "Male"),
        (FEMALE, "Female")
    )

    photo = models.ImageField(blank=True, upload_to=get_path)
    gender = models.CharField(max_length=1, choices=genders, default=FEMALE)
    birth_date = models.DateField(null=True)
    city = models.CharField(max_length=40, blank=True)
    country = models.CharField(max_length=40, blank=True)
    phone_number = models.CharField(max_length=20, blank=True)

    favourite_rooms = models.ManyToManyField('room.Room', related_name="users_favour_list")
    visited_rooms = models.ManyToManyField('room.Room', related_name="users_visit_list")

    def __unicode__(self):
        return self.user.username


class Contact(models.Model):
    class Meta:
        db_table = "contacts"
        verbose_name_plural = "contacts"
        unique_together = ('user', 'contact')

    user = models.ForeignKey(User, related_name="contacts_list")
    contacts_type = (
        ("VK", "vk"),
        ("FACEBOOK", "facebook"),
        ("TWITTER", "twitter"),
        ("SKYPE", "skype")
    )
    contact = models.CharField(max_length=8, choices=contacts_type, default="VK")
    value = models.CharField(max_length=40)

    def __unicode__(self):
        return "%s: %s" % (self.contact, self.value)


class Review(models.Model):
    class Meta:
        db_table = "review"
        verbose_name_plural = "reviews"

    author = models.ForeignKey(User, related_name="reviews_list", null=True)  # user, who added comment
    room = models.ForeignKey('room.Room', related_name="reviews_list", null=True)
    text = models.TextField(max_length=250)
    pub_date = models.DateTimeField()

    def __unicode__(self):
        return "%s %s %s" % (self.author.username, self.pub_date, self.text)
