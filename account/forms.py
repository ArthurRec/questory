from django import forms
from django.contrib.auth.models import User
from django.core.validators import RegexValidator
from django.forms import PasswordInput, Textarea, TextInput

from account.models import Profile, Review


class LoginForm(forms.Form):
    login = forms.CharField(widget=forms.TextInput(attrs={'class': "form-control not-dark", 'name': "login"}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': "form-control not-dark", 'name': "password"}))


class RegisterForm(forms.ModelForm):
    confirm_password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={'class': "form-control not-dark", 'id': "conf_password", 'name': "conf_password"}))

    class Meta:
        model = User
        fields = ["username", "email", "password"]

        widgets = {
            "email": TextInput(attrs={'class': "form-control not-dark", 'name': "email"}),
            "username": TextInput(attrs={'class': "form-control not-dark", 'name': "username"}),
            "password": PasswordInput(attrs={'class': "form-control not-dark", 'id': "password", 'name': "password"})
        }

    def clean(self):
        cleaned_data = self.cleaned_data
        password = cleaned_data.get("password")
        confirm_password = cleaned_data.get("confirm_password")
        if password != confirm_password:
            raise forms.ValidationError("Passwords must match.")
        return cleaned_data

    def clean_username(self):
        username = self.cleaned_data['username']
        if User.objects.filter(username=username).exists():  # .exclude(pk=self.instance.pk)
            raise forms.ValidationError(u'Username "%s" is already in use.' % username)
        return username


class EditProfileForm(forms.ModelForm):
    phone_number = forms.CharField(validators=[RegexValidator(
        r"\+7-\d{3}-\d{3}-\d{2}-\d{2}", "Wrong format!!!"
    )])

    class Meta:
        model = Profile  # User
        fields = ["photo", "gender", "birth_date", "city", "country"]


class ReviewForm(forms.ModelForm):
    class Meta:
        model = Review
        fields = ["text"]

        widgets = {
            "text": Textarea(
                attrs={'id': 'commentArea', 'required': True, 'placeholder': 'Your Comment...'}
            ),
        }
